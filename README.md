# weezplay

## Build

### Dependencies

On a Debian system, use the following command to install development
dependencies:

```shell
sudo apt install \
    libyaml-cpp-dev libdocopt-dev libglfw3-dev \
    linopencv-dev \
    libre2-dev
```

### Compiling

```shell
make configure
make release
```

### Starting sample application

```shell
./build/release/bin/weezplay/weezplay
```
