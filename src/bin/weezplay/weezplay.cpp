#include <iostream>

#include <weezplay/gui/app.hpp>
#include <weezplay/gui/menu.hpp>
#include <weezplay/gui/video.hpp>

int main(int ac, char** av)
{
    weezplay::gui::app a;

    a.add_elements<
        weezplay::gui::menu,
        weezplay::gui::video
    >();

    a.init();
    a.run();

    return 0;
}
