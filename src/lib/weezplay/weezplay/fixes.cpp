#include <charconv>

#include <csv2/csv2.hpp>

#include <weezplay/fixes.hpp>
#include <weezplay/types.hpp>
#include <weezplay/log.hpp>

namespace weezplay {

template <typename T>
void
conv_num(const std::string& s, T& v)
{
    std::string_view sv(s);

    die_unless(
        std::from_chars(sv.begin(), sv.end(), v).ec == std::errc{},
        "failed to convert ", s
    );
}

fix_map
read_fixes(const std::string& file)
{
    using csv_reader = csv2::Reader<
        csv2::delimiter<';'>,
        csv2::quote_character<'"'>,
        csv2::first_row_is_header<true>,
        csv2::trim_policy::trim_whitespace
    >;

    csv_reader csv;

    die_unless(csv.mmap(file), "failed to open ", file);

    std::size_t line = 0;
    std::size_t cols = 6;

    fix_map fixes;

    for (const auto row: csv) {
        strings frow;

        for (const auto cell: row) {
            std::string value;
            cell.read_value(value);

            frow.emplace_back(std::move(value));
        }

        if (frow.size() != cols) {
            // Ignore empty/incorrect line
            continue;
        }

        fix f;
        std::size_t col = 0;

        conv_num(frow[col++], f.time);
        conv_num(frow[col++], f.v_pulse);
        conv_num(frow[col++], f.y_pulse);
        conv_num(frow[col++], f.z_pulse);
        conv_num(frow[col++], f.current_segment_index);
        conv_num(frow[col++], f.progress_ratio);

        fixes.try_emplace(f.time, std::move(f));

        ++line;
    }

    return fixes;
}

}
