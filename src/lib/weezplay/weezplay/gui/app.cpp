#define IMGUI_DEFINE_MATH_OPERATORS
#include <imgui_impl_glfw.h>
#include <imgui_impl_opengl3.h>

#include <iostream>

#include <weezplay/gui/app.hpp>
#include <weezplay/log.hpp>

namespace weezplay::gui {

static
void
glfw_error_callback(int error, const char* description)
{
    std::cerr << "GLFW Error: " << error << description << std::endl;
}

app::app()
: es_(eq_)
{}

app::~app()
{
    ImGui_ImplOpenGL3_Shutdown();
    ImGui_ImplGlfw_Shutdown();
    ImGui::DestroyContext();

    wp_.reset();

    glfwTerminate();
}

void
app::init()
{
    glfwSetErrorCallback(glfw_error_callback);

    die_unless(glfwInit(), "failed to initialize GLFW");

    // GL 3.0 + GLSL 130
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
    //glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);  // 3.2+ only
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);            // 3.0+ only

    wp_ = new_window(width_, height_, "weezplay", nullptr, nullptr);

    die_unless(wp_, "failed to create GLFW window");

    auto w = wp_.get();

    glfwMakeContextCurrent(w);
    glfwSwapInterval(1); // Enable vsync

    // Setup Dear ImGui context
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGuiIO& io = ImGui::GetIO(); (void)io;
    //io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;     // Enable Keyboard Controls
    //io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;      // Enable Gamepad Controls

    // Setup Dear ImGui style
    ImGui::StyleColorsDark();
    //ImGui::StyleColorsLight();

    // Setup Platform/Renderer backends
    ImGui_ImplGlfw_InitForOpenGL(w, true);

    const char* glsl_version = "#version 130";
    ImGui_ImplOpenGL3_Init(glsl_version);
}

void
app::run()
{
    auto w = wp_.get();

    while (run_) {
        // Poll and handle events (inputs, window resize, etc.)
        // You can read the io.WantCaptureMouse, io.WantCaptureKeyboard flags to tell if dear imapp wants to use your inputs.
        // - When io.WantCaptureMouse is true, do not dispatch mouse input data to your main application, or clear/overwrite your copy of the mouse data.
        // - When io.WantCaptureKeyboard is true, do not dispatch keyboard input data to your main application, or clear/overwrite your copy of the keyboard data.
        // Generally you may always pass all inputs to dear imapp, and hide them from your application based on those two flags.
        glfwPollEvents();

        new_frame();

        es_();

        render();

        process_events();

        if (glfwWindowShouldClose(w)) {
            run_ = false;
        }
    }
}

void
app::new_frame()
{
    // Start the Dear ImGui frame
    ImGui_ImplOpenGL3_NewFrame();
    ImGui_ImplGlfw_NewFrame();
    ImGui::NewFrame();
}

void
app::render()
{
    auto w = wp_.get();

    // Rendering
    ImGui::Render();

    int display_w, display_h;
    glfwGetFramebufferSize(w, &display_w, &display_h);
    glViewport(0, 0, display_w, display_h);

    glClearColor(
        clear_color_.x * clear_color_.w,
        clear_color_.y * clear_color_.w,
        clear_color_.z * clear_color_.w,
        clear_color_.w
    );

    glClear(GL_COLOR_BUFFER_BIT);

    ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

    glfwSwapBuffers(w);
}

void
app::process_events()
{
    eq_.process_events([&](const auto& e) {
        if (e.id == event_id::QUIT) {
            run_ = false;
            return true;
        }

        return false;
    });
}

}
