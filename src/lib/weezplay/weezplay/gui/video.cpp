#include <algorithm>

#include <opencv2/opencv.hpp>

#include <weezplay/gui/video.hpp>
#include <weezplay/utils.hpp>
#include <weezplay/log.hpp>
#include <weezplay/types.hpp>

namespace weezplay::gui {

video::video(event_queue& eq)
: element_base(eq)
{}

video::~video()
{
    frame_.release();
    vc_.release();
}

void
video::operator()()
{
    if (vc_.isOpened()) {
        next_frame();
    }

    render_frame();
    process_events();
}

void
video::next_frame()
{
    auto now = clock::now();
    elapsed_duration_ = std::chrono::duration_cast<duration>(now - start_time_);
    frame_changed_ = false;

    apply_fix();

    if (now > (last_frame_time_ + frame_duration_)) {
        last_frame_time_ = now;

        if (!vc_.read(in_frame_)) {
            vc_.release();
        } else {
            make_texture();
        }
    }
}

void
video::render_frame()
{
    if (!has_texture_) {
        return;
    }

    ImGui::SetNextWindowSize(win_dims_);

    if (ImGui::Begin("Video", &show_video_)) {
        ImGui::Value("Elapsed time (ms)", (unsigned int) elapsed_duration_.count());
        ImGui::Value("Speed (cm/mn)    ", v_pulse_);
        ImGui::Value("Y offset         ", y_pulse_);
        ImGui::Value("Z offset         ", z_pulse_);

        ImGui::Image(
            reinterpret_cast<void*>(static_cast<intptr_t>(texture_)),
            frame_dims_
        );
    }

    ImGui::End();
}

void
video::apply_fix()
{
    if (fix_it_ == fixes_.end()) {
        return;
    }

    std::size_t ms = elapsed_duration_.count();
    const auto& f = fix_it_->second;

    if (f.time <= ms) {
        v_pulse_ += f.v_pulse;
        y_pulse_ += f.y_pulse;
        z_pulse_ += f.z_pulse;
        ++fix_it_;
    }
}

void
video::open(const std::string& path)
{
    open_fixes(path);
    open_video(path);

    /*auto trajectory = first_file(path, "trajectory.yml");
    log("trajectory: ", trajectory);
    auto conf = first_file(path, "welding-config.yml");
    log("conf: ", conf);*/
}

void
video::open_fixes(const std::string& path)
{
    auto fixes_file = first_file(path, "*.csv");

    fixes_ = read_fixes(fixes_file);
    fix_it_ = fixes_.begin();
    v_pulse_ = 0;
    y_pulse_ = 0;
    z_pulse_ = 0;
}

void
video::open_video(const std::string& path)
{
    auto avi = first_file(path, "*.avi");

    vc_.open(avi);
    start_time_ = clock::now();
    last_frame_time_ = start_time_;

    std::size_t fps = vc_.get(cv::CAP_PROP_FPS);
    duration d = std::chrono::seconds(1);
    frame_duration_ = d / fps;

    die_unless(vc_.isOpened(), "failed to open ", avi);
}

void
video::make_texture()
{
    cv::resize(in_frame_, frame_, cv::Size(), 0.5, 0.5, cv::INTER_AREA);
    cv::cvtColor(frame_, frame_, cv::COLOR_BGR2RGBA);

    glGenTextures(1, &texture_);
    glBindTexture(GL_TEXTURE_2D, texture_);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glPixelStorei(GL_UNPACK_ROW_LENGTH, 0);
    glTexImage2D(
        GL_TEXTURE_2D,
        0,
        GL_RGBA,
        frame_.cols,
        frame_.rows,
        0,
        GL_RGBA,
        GL_UNSIGNED_BYTE,
        frame_.data
    );

    has_texture_ = true;
    frame_dims_ = ImVec2(frame_.cols, frame_.rows);
    win_dims_ = frame_dims_;
    win_dims_.y += 200;
}

std::string
video::first_file(
    const std::string& path,
    const std::string& file
)
{
    auto f = cat_file(path, file);

    if (file.starts_with('*')) {
        auto files = glob(f);

        die_unless(files.size() == 1, "not unique: ", file);

        return files.front();
    } else if (file_exists(f)) {
        return f;
    }

    die("no '", file, "' here: ", path);

    return {};
}

void
video::process_events()
{
    element_base::process_events([&](const auto& e) {
        if (e.id == event_id::OPEN_RUN) {
            open(e.data);
            return true;
        }

        return false;
    });
}

}
