#include <weezplay/gui/menu.hpp>
#include <weezplay/log.hpp>

namespace weezplay::gui {

menu::menu(event_queue& eq)
: element_base(eq)
{}

menu::~menu()
{}

void
menu::operator()()
{
    if (ImGui::BeginMainMenuBar()) {
        if (ImGui::BeginMenu("File")) {
            file();
            ImGui::EndMenu();
        }

        ImGui::EndMainMenuBar();
    }

    if (open_run_) {
        open_run();
    }
}

void
menu::open_run()
{
    // display
    if (ImGuiFileDialog::Instance()->Display("OpenRunDlgKey")) {
        // action if OK
        if (ImGuiFileDialog::Instance()->IsOk()) {
            post_event(event{
                .id = event_id::OPEN_RUN,
                .data = ImGuiFileDialog::Instance()->GetCurrentPath()
            });
        }

        // close
        ImGuiFileDialog::Instance()->Close();
    }
}

void
menu::file()
{
    open_run_ = false;

    if (ImGui::MenuItem("Open run...", "Ctrl+O")) {
        open_run_ = true;

        ImGuiFileDialog::Instance()->OpenDialog(
            "OpenRunDlgKey",
            "Choose Run directory",
            nullptr, // No file ext
            ".",
            1,
            nullptr,
            ImGuiFileDialogFlags_Modal
        );
    }

    ImGui::Separator();

    if (ImGui::MenuItem("Quit", "Ctrl+Q")) {
        post_event(event_id::QUIT);
    }
}

}
