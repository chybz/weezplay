#include <weezplay/gui/event.hpp>

namespace weezplay::gui {

bool
event_queue::has_events() const
{ return !q.empty(); }

bool
event_queue::empty() const
{ return q.empty(); }

void
event_queue::process_events(event_cb cb)
{
    if (!has_events()) {
        return;
    }

    const auto& e = next_event();

    if (cb(e)) {
        pop();
    }
}

const event&
event_queue::next_event() const
{ return q.front(); }

void
event_queue::pop()
{ q.pop(); }

}
