#include <weezplay/gui/element.hpp>

namespace weezplay::gui {

element_base::element_base(event_queue& eq)
: eq_(eq)
{}

element_base::~element_base()
{}

void
element_base::post_event(event_id id)
{ post_event(event{ .id = id }); }

void
element_base::process_events(event_queue::event_cb cb)
{ eq_.process_events(cb); }

elements::elements(event_queue& eq)
: eq_(eq)
{}

void
elements::operator()()
{
    for (auto&& ep : ptrs) {
        (*ep)();
    }
}

}
