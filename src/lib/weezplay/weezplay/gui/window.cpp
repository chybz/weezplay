#include <weezplay/gui/window.hpp>
#include <weezplay/log.hpp>

namespace weezplay::gui {

void
window_init(window_ptr wp)
{
    auto p = wp.get();

    glfwMakeContextCurrent(p);
    glfwSwapInterval(1); // Enable vsync
}

}
