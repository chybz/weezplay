#pragma once

#include <weezplay/gui/event.hpp>
#include <weezplay/gui/window.hpp>
#include <weezplay/gui/element.hpp>

namespace weezplay::gui {

void
gui_init();

window_ptr
app_init(const char* title, std::size_t width, std::size_t height);

class app
{
public:
    app();
    ~app();

    template <typename... Elements>
    void add_elements()
    { es_.add<Elements...>(); }

    void init();
    void run();

private:
    void new_frame();
    void render();
    void process_events();

    std::size_t width_ = 800;
    std::size_t height_ = 600;
    bool run_ = true;
    ImVec4 clear_color_ = { 0.45f, 0.55f, 0.60f, 1.00f };

    window_ptr wp_ = { nullptr, nullptr };

    event_queue eq_;
    elements es_;
};

}
