#pragma once

#include <memory>
#include <vector>

#include <weezplay/gui/imgui.hpp>
#include <weezplay/gui/event.hpp>

namespace weezplay::gui {

class element_base
{
public:
    element_base(event_queue& eq);
    virtual ~element_base();

    virtual void operator()() = 0;

protected:
    void post_event(event_id id);

    template <typename Event>
    void post_event(Event&& e)
    { eq_.post_event(std::move(e)); }

    void process_events(event_queue::event_cb cb);

private:
    event_queue& eq_;
};

using element_ptr = std::unique_ptr<element_base>;

template <typename T, typename... Args>
element_ptr
new_element(Args&&... args)
{ return std::make_unique<T>(std::forward<Args>(args)...); }

using element_ptrs = std::vector<element_ptr>;

struct elements
{
    elements(event_queue& eq);

    template <typename... Elements>
    void add()
    { (ptrs.emplace_back(new_element<Elements>(eq_)), ...); }

    void operator()();

    element_ptrs ptrs;
    event_queue& eq_;
};

}
