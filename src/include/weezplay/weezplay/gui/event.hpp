#pragma once

#include <string>
#include <queue>
#include <functional>

namespace weezplay::gui {

enum class event_id
{
    QUIT,
    OPEN_RUN
};

struct event
{
    event_id id;
    std::string data;
};

struct event_queue
{
    using queue_type = std::queue<event>;
    using event_cb = std::function<bool(const event& e)>;

    bool empty() const;
    bool has_events() const;

    void process_events(event_cb cb);

    template <typename Event>
    void post_event(Event&& e)
    { q.push(std::move(e)); }

    const event& next_event() const;
    void pop();

    queue_type q;
};



}
