#pragma once

#include <weezplay/gui/element.hpp>

#include <ImGuiFileDialog.h>

namespace weezplay::gui {

class menu : public element_base
{
public:
    menu(event_queue& eq);
    virtual ~menu();

    void operator()() override final;

    void open_run();
    void file();

private:
    bool open_run_;
    ImGuiFileDialog file_dialog_;
};

}
