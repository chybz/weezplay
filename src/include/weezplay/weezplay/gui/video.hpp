#pragma once

#include <string>
#include <chrono>

#include <opencv2/videoio.hpp>

#include <GLFW/glfw3.h>

#include <weezplay/gui/element.hpp>
#include <weezplay/fixes.hpp>

namespace weezplay::gui {

class video : public element_base
{
public:
    video(event_queue& eq);
    virtual ~video();

    void operator()() override final;

private:
    using clock = std::chrono::high_resolution_clock;
    using time_point = typename clock::time_point;
    using duration = std::chrono::milliseconds;

    void next_frame();
    void render_frame();

    void apply_fix();

    void make_texture();

    void open(const std::string& path);
    void open_fixes(const std::string& path);
    void open_video(const std::string& path);

    std::string first_file(
        const std::string& path,
        const std::string& ext
    );

    void process_events();

    cv::VideoCapture vc_;
    cv::Mat in_frame_;
    cv::Mat frame_;
    GLuint texture_;
    bool has_texture_ = false;
    ImVec2 frame_dims_;
    ImVec2 win_dims_;
    std::size_t frame_nb_ = 0;
    time_point last_frame_time_;
    duration frame_duration_;
    time_point start_time_;
    duration elapsed_duration_;
    bool frame_changed_ = false;
    bool show_video_ = true;

    int v_pulse_ = 0;
    int y_pulse_ = 0;
    int z_pulse_ = 0;
    weezplay::fix_map fixes_;
    weezplay::fix_map::const_iterator fix_it_;
};

}
