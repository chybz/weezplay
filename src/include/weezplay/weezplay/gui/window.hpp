#pragma once

#include <GLFW/glfw3.h>

#include <memory>

namespace weezplay::gui {

using window_type = GLFWwindow;
using window_raw_ptr = window_type*;

inline
void
destroy_window(window_raw_ptr p)
{ glfwDestroyWindow(p); }

using window_ptr = std::unique_ptr<window_type, decltype(&destroy_window)>;

template <typename... Args>
window_ptr
new_window(Args&&... args)
{
    return window_ptr(
        glfwCreateWindow(std::forward<Args>(args)...),
        &destroy_window
    );
}

void
window_init(window_ptr p);

}
