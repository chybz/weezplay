#pragma once

#include <map>

namespace weezplay {

struct fix
{
    std::size_t time;
    int v_pulse;
    int y_pulse;
    int z_pulse;
    std::size_t current_segment_index;
    float progress_ratio;
};

using fix_map = std::map<std::size_t, fix>;

fix_map
read_fixes(const std::string& file);

}
