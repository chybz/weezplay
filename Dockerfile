###############################################################################
#
# Base build image stage
#
###############################################################################
from debian:testing-slim as base

RUN apt-get update && apt-get install -y --no-install-recommends \
# Compiler
    g++ \
# Dev tools
    make \
    cmake cmake-data cmake-extras \
# Download and extract
    ca-certificates \
    tar gzip unzip curl \
# Base dev dependencies
    xorg-dev \
# Clean the cache
    && rm -rf /var/lib/apt/lists/*

WORKDIR /source

###############################################################################
#
# Dependencies
#
###############################################################################
FROM base as deps

COPY deps.txt .
COPY Makefile .
COPY toolchains toolchains
COPY utils utils

RUN make externals

###############################################################################
#
# Build
#
###############################################################################
FROM deps as build
